
//Robo-Mate project
//EXOSHOE - FSR sensors 
//------------------------------------------------
//Luis A. Mateos
//@IIT 25/06/2015

//
// @IIT 10/08/2015
// Read 8 sensors from one exoshoe in parallel

// @IIT 28/11/2015
// Read 16 sensors from insole and exosole

// @IIT 24/03/2016
// Test the final setup on shoe with arduino NANO

// @IIT 29/03/2016
// Write fitting curve function to obtain KG


//#include "definitions.h"
//#include "right.h"
//#include "left.h"
//#include "FSR.h"



//==================================
//define FSR
//==================================

#define R2 5600 //2198 
#define VIN 3.3 
#define ADCSTEPS 1024 
#define TIMECONSTANT 5 //miliseconds 
#define SENSORS 8 // NUMBER of FSR sensors in FOOT 
//#define ELEMENTS 7 // for fittingCurve arrays 
#define ELEMENTS 22 //24 // for fittingCurve arrays 

/*
 * NOTES:
 * MAX RESISTANCE from FSR sensor is set to INT 32767 and not to 2248554, since we can work with INT array
 * INT 32767
 * unsigned INT 65535
 * MAX R = 2248554
 */
#define MAXRSENSOR 100000 //32767 // MAX R for FSRs 



// LEFT BACK FSR sensors
 //     kg              {1,           1000,  2000, 3000, 4000, 5000, 6000, 7000, 8000, 9000, 10000,11000,12000,13000,14000,15000,16000,17000,18000,19000,20000,  30000, 40000, 45000 }
   long sib1[ELEMENTS] = {MAXRSENSOR, 29800, 10690, 5938, 4005, 2946, 2353, 1945, 1677, 1497, 1368, 1268, 1194, 1131, 1068, 1022,  984,  954,  931,  902, 887,  786};//,  722,  695 };//s9
   long sib2[ELEMENTS] = {MAXRSENSOR, 36560, 12900, 7705, 5364, 4304, 3590, 3128, 2784, 2534, 2364, 2170, 2077, 1975, 1876, 1809, 1742, 1686, 1650, 1595, 1559, 1317};//, 1186, 1115};//s18
   long sib3[ELEMENTS] = {MAXRSENSOR, 35650,  11940, 6706, 4496, 3374, 2796, 2398, 2097, 1886, 1705, 1595, 1488, 1410, 1334, 1276, 1210, 1178, 1138, 1083, 1037, 887};//,  800,   757};//s11
   long sib4[ELEMENTS] = {MAXRSENSOR, 48500, 13710, 7830, 5261, 4054, 3318, 2908, 2592, 2353, 2149, 1985, 1857, 1752, 1659, 1595, 1568, 1480, 1436, 1410, 1359, 1154};//, 1037, 984 };//s10

// LEFT FRONT FSR sensors
 //     kg               {1,       1000,  2000, 3000, 4000, 5000, 6000, 7000, 8000, 9000, 10000,11000,12000,13000,14000,15000,16000,17000,18000,19000,20000,  30000, 40000, 45000 }
   long sif1[ELEMENTS] = {MAXRSENSOR, 51180, 16200, 9066, 6224, 4770, 3816, 3277, 2821, 2534, 2299,  2118, 1985, 1876, 1780, 1686, 1613, 1559, 1506, 1462, 1427,   1194};//,   1076,    1037};//s4 
   long sif2[ELEMENTS] = {MAXRSENSOR, 31160, 11220, 6732, 4807, 3801, 3236, 2833, 2557, 2387, 2213,  2087, 1995, 1886, 1847, 1790, 1733, 1686, 1640, 1613, 1568,   1359};//,   1268,    1243};//s3
   long sif3[ELEMENTS] = {MAXRSENSOR, 18100, 5666, 3531, 2545, 2015, 1705, 1506, 1351, 1243, 1146,  1107, 1029, 984, 946, 916, 887, 865, 858, 836, 814,   736};//,   695,    681};//s5
   long sif4[ELEMENTS] = {MAXRSENSOR, 22370, 6248, 3910, 2895, 2299, 1935, 1686, 1515, 1376, 1276,  1210, 1138, 1076, 1022, 961, 939, 924, 872, 850, 822,   702};//,   626,    613};//s8  XXX



//// RIGHT BACK FSR sensors
// //    kg                 {1,           1000,  2000, 3000, 4000, 5000, 6000, 7000, 8000, 9000, 10000,11000,12000,13000,14000,15000,16000,17000,18000,19000,20000,  30000, 40000, 45000 }
//   long sib1 [ELEMENTS] = {MAXRSENSOR,  42590, 11270, 6055, 4103, 3023, 2477, 2087, 1847, 1659, 1515,  1376, 1301, 1243, 1186, 1123, 1091, 1060,  1029,  999,  961,   822};//,   736,   709};//s6
//   long sib2 [ELEMENTS] = {MAXRSENSOR,  40650, 17430, 9815, 6575, 4751, 3755, 3155, 2723, 2387, 2128,  1925, 1771, 1622, 1550, 1445, 1402, 1326,  1268,  1210,  1186,   954};//,   850,   807};//s1
//   long sib3 [ELEMENTS] = {MAXRSENSOR,  60310, 16540, 9256, 6472, 4903, 3894, 3277, 2796, 2477, 2213,  2036, 1886, 1761, 1650, 1550, 1488, 1427,  1359,  1309,  1284,   1068};//,   946,   916};//s14
//   long sib4 [ELEMENTS] = {MAXRSENSOR,  34220, 14310, 8665, 6032, 4514, 3560, 2946, 2545, 2160, 1896,  1714, 1559, 1436, 1342, 1235, 1170, 1099,  1060,  1029,  991,   807};//,   709,   688};//s16
//
//// RIGHT FRONT FSR sensors
// //    kg                 {1,       1000,  2000, 3000, 4000, 5000, 6000, 7000, 8000, 9000, 10000,11000,12000,13000,14000,15000,16000,17000,18000,19000,20000,  30000, 40000, 45000 }
//   long sif1 [ELEMENTS] = {MAXRSENSOR, 33410, 10970, 5846, 3910, 2895, 2277, 1945, 1677, 1515, 1376,  1284, 1202, 1131, 1083, 1022, 984, 961, 916, 894, 872,   765};//,   695,   660 };//s13  XXX
//   long sif2 [ELEMENTS] = {MAXRSENSOR, 29800, 10370, 5915, 3941, 2883, 2299, 1925, 1705, 1541, 1402,  1317, 1235, 1178, 1131, 1107, 1037, 1006, 991, 961, 946,   836};//,   772,    750};//s2 miint sing a
//   long sif3 [ELEMENTS] = {MAXRSENSOR, 30020, 11520, 6732, 4751, 3679, 3036, 2569, 2245, 2036, 1857,  1714, 1586, 1515, 1436, 1368, 1317, 1268, 1227, 1186, 1154,   976};//,   858,    829};//s7
//   long sif4 [ELEMENTS] = {MAXRSENSOR, 38510, 6839, 4270, 3249, 2699, 2353, 2128, 1965, 1838, 1742,  1686, 1586, 1541, 1488, 1462, 1427, 1410, 1393, 1368, 1334,  1210 };//,   1138,    1107};//s17


byte startByte=0xAE;
unsigned int foot = 0;
unsigned char checksum=0;
byte buf[4] =   {0,0,0,0}; // 

//==================================
//VARIABLES
//==================================
int counter = 0; 
//                            back     front
//                           Rb1 -> 4  Rf1 -> 4
//                           Lb1 -> 4  Lf1 -> 4
int SensorValue[SENSORS] =   {0,0,0,0, 0,0,0,0 }; // values 0-1024 from ADC
float SensorR[SENSORS] =     {0,0,0,0, 0,0,0,0 }; // RESISTANCE
float SensorGrams[SENSORS] = {0,0,0,0, 0,0,0,0 }; // grams
float TotaliB=0; // Total weight on foot-back FSR (heel)
float TotaliF=0; // Total weight on foot-front (metatarsals)
float Total=0; // Total weight from the foot
// kg Tables for fitting function
long kilos[ELEMENTS] = {1, 1000,2000,3000,4000,5000,6000,7000,8000,9000,10000,11000,12000,13000,14000,15000,16000,17000,18000,19000,20000, 30000};//, 40000, 45000};//grams

//==================================
//SETUP
//==================================
void setup() {
  //INTERNAL: an built-in reference, equal to 1.1 volts on the ATmega168 or ATmega328 and 
  //2.56 volts on the ATmega8 (not available on the Arduino Mega)
  analogReference(EXTERNAL); //DEFAULT, INTERNAL, INTERNAL1V1, INTERNAL2V56, or EXTERNAL
//  Serial.begin(57600);
  Serial.begin(115200);
}

//==================================
//LOOP
//==================================
void loop() {
//===========================================
//SENSORS READING
//-------------------------------------------
// GET the data from each FSR sensor
 SensorValue[0] = analogRead(A0); //LB1 FOOT BACK
 SensorValue[1] = analogRead(A1); //LB2
 SensorValue[2] = analogRead(A2); //LB3
 SensorValue[3] = analogRead(A3); //LB4

 SensorValue[4] = analogRead(A4); //LF1 FOOT FRONT
 SensorValue[5] = analogRead(A5); //LF2
 SensorValue[6] = analogRead(A6); //LF3
 SensorValue[7] = analogRead(A7); //LF4

// CONVERT the DATA to OHMS
                //calculateGrams(float value, int VINPUT, int ADC, int R22)
 SensorR[0] = calculateFSR_R(SensorValue[0], VIN, ADCSTEPS, R2); //LB1 LEFT BACK
 SensorR[1] = calculateFSR_R(SensorValue[1], VIN, ADCSTEPS, R2); //LB2
 SensorR[2] = calculateFSR_R(SensorValue[2], VIN, ADCSTEPS, R2); //LB3
 SensorR[3] = calculateFSR_R(SensorValue[3], VIN, ADCSTEPS, R2); //LB4

 SensorR[4] = calculateFSR_R(SensorValue[4], VIN, ADCSTEPS, R2); //LF1 LEFT FRONT
 SensorR[5] = calculateFSR_R(SensorValue[5], VIN, ADCSTEPS, R2); //LF2
 SensorR[6] = calculateFSR_R(SensorValue[6], VIN, ADCSTEPS, R2); //LF3
 SensorR[7] = calculateFSR_R(SensorValue[7], VIN, ADCSTEPS, R2); //LF4

// CONVERT the DATA to kg GRAMS
 SensorGrams[0] = fittingCurve2(SensorR[0],  sib1, kilos); // Obtain the kg from fitting curve RESISTANCE -> KG 
 SensorGrams[1] = fittingCurve2(SensorR[1],  sib2, kilos); // Obtain the kg from fitting curve RESISTANCE -> KG 
 SensorGrams[2] = fittingCurve2(SensorR[2],  sib3, kilos); // Obtain the kg from fitting curve RESISTANCE -> KG 
 SensorGrams[3] = fittingCurve2(SensorR[3],  sib4, kilos); // Obtain the kg from fitting curve RESISTANCE -> KG 
TotaliB=SensorGrams[0]+SensorGrams[1]+SensorGrams[2]+SensorGrams[3];
 SensorGrams[4] = fittingCurve2(SensorR[4],  sif1, kilos); // Obtain the kg from fitting curve RESISTANCE -> KG 
 SensorGrams[5] = fittingCurve2(SensorR[5],  sif2, kilos); // Obtain the kg from fitting curve RESISTANCE -> KG 
 SensorGrams[6] = fittingCurve2(SensorR[6],  sif3, kilos); // Obtain the kg from fitting curve RESISTANCE -> KG 
 SensorGrams[7] = fittingCurve2(SensorR[7],  sif4, kilos); // Obtain the kg from fitting curve RESISTANCE -> KG 
TotaliF=SensorGrams[4]+SensorGrams[5]+SensorGrams[6]+SensorGrams[7];

Total=TotaliB+TotaliF;


checksum=0;
foot=(unsigned int)Total;

//unsigned long  decenas = 0;
//unsigned long  unidades = 0;


buf[0] = startByte;
buf[1] = (byte)((foot >> 8) & 0xFF);
buf[2] = (byte)(foot & 0xFF);

checksum = checksum ^ buf[0];
checksum = checksum ^ buf[1];
checksum = checksum ^ buf[2];

buf[3] = (byte)checksum;

Serial.write(buf,sizeof(buf));

// ==========================================================================
//PRINT 
//---------------------------------------------------------------------------
//INSOLE BACK
//  Serial.print(SensorValue[0]);
//   Serial.print(",");    
//  Serial.print(SensorR[0]);
//   Serial.print(",");    
//  Serial.print(SensorGrams[0]);
//   Serial.print(",    ");    

/*
  Serial.print(SensorValue[0]);
   Serial.print(",");    
  Serial.print(SensorValue[1]);
   Serial.print(",");    
  Serial.print(SensorValue[2]);
   Serial.print(",");    
  Serial.print(SensorValue[3]);
   Serial.print(",  ");    

  Serial.print(SensorValue[4]);
   Serial.print(",");    
  Serial.print(SensorValue[5]);
   Serial.print(",");    
  Serial.print(SensorValue[6]);
   Serial.print(",");    
  Serial.print(SensorValue[7]);
   Serial.print(",   ");    

  Serial.print(SensorR[0]);
   Serial.print(",");    
  Serial.print(SensorR[1]);
   Serial.print(",");    
  Serial.print(SensorR[2]);
   Serial.print(",");    
  Serial.print(SensorR[3]);
   Serial.print(",  ");    

  Serial.print(SensorR[4]);
   Serial.print(",");    
  Serial.print(SensorR[5]);
   Serial.print(",");    
  Serial.print(SensorR[6]);
   Serial.print(",");    
  Serial.print(SensorR[7]);
   Serial.print(", =  ");    
 
  Serial.print(SensorGrams[0]);
   Serial.print(",");    
  Serial.print(SensorGrams[1]);
   Serial.print(",");    
  Serial.print(SensorGrams[2]);
   Serial.print(",");    
  Serial.print(SensorGrams[3]);
   Serial.print(",  ");    

  Serial.print(SensorGrams[4]);
   Serial.print(",");    
  Serial.print(SensorGrams[5]);
   Serial.print(",");    
  Serial.print(SensorGrams[6]);
   Serial.print(",");    
  Serial.print(SensorGrams[7]);
   Serial.print(", =  ");    

  Serial.print(TotaliB);
   Serial.print(",");    
  Serial.print(TotaliF);
   Serial.print(",  =  ");    
  Serial.print(Total);
   Serial.print(",   TIME=");    
 */ 
 /*
   //Serial.print(Total); 
   Serial.print(Total); 
    Serial.print(",");    // ,
   Serial.print(foot); 
    //Serial.print(",");    // ,
 Serial.println();
*/
 // delay(TIMECONSTANT); //wait 50ms
  counter++;
}


//==================================
//FUNCTIONS
//==================================

/*
 *  FITTING CURVE 
 * axis -  variable   value
 * X    -> fsr     -> RESISTANCE in OHMs 
 * Y    -> kg      -> grams
 * 
 * a initial fsr RESISTANCE
 * c final fsr RESISTANCE
 * b fsr RESISTANCE
 * 
 * fa = grams @ initial a
 * bc = grams @ final c
 * fb = grams from fsr RESISTANCE
 * 
 * SENSOR R    {10000,7783,4838,3571,2818,2371,1598};//s9
 * KILOS grams {1,    1000,2000,3000,4000,5000,10000};//grams
 */
float fittingCurve2(float b, long fsr[], long kg[]){ 
  float fb; // VALUE to return -> GRAMS from the FSR sensor
  long a; // lower limit R
  long c; // higher limit R
  long fa; // lower limit kg
  long fc; // higher limit kg
  long bMAX=0; // Max R from sensor to be taken into account
  long bMIN=0; // Min R from sensor to be taken into account
  long fbMAX=0; // Max kg from sensor to be taken into account
  long fbMIN=0; // Min kg from sensor to be taken into account
  int i; // counter
  int j; // counter to set lower limit a
  int k; // counter to set higher limit c

  // SET MAX and min values
  bMAX = fsr[0];//kg[sizeof(kg)-1];
  bMIN = fsr[ELEMENTS-1];//kg[0];
  fbMIN = kg[0];//kg[sizeof(kg)-1];
  fbMAX = kg[ELEMENTS-1];//kg[0];

  // IF values out of range then SET the limits
  if (b>bMAX)  {  // SET MAX and MIN allowed by the fitting curve function from FSR sensor data
    fb = fbMIN;  } // kg values no bigger than MAX
  // IF values out of range then SET the limits
  else if (b<bMIN) {  // SET MAX and MIN allowed by the fitting curve function from FSR sensor data
    fb = fbMAX; } // kg values no less than MIN
  // FittingCurve function from values in the limits    
  else{ // SELECT the right interval to obtain the kg
    for (i = 0; i < ELEMENTS; i ++) { // Looping the resistance values
      if (b<=fsr[i]) { j = i;} // IF FSR resistance value is bigger than save a //a fsr[i];} //a
      if (b>=fsr[i]) { k = i; break;} // IF FSR resistance value is less than save c and break, since we collected a and c //c k = fsr[i]; break;} //c
    }
  
    // POPULATE DATA
    a = fsr[j];
    c = fsr[k];
    fa = kg[j];
    fc = kg[k];
      
    // Calculate the fitting function   
    fb = fa + ((b-a)/(c-a))*(fc-fa);
  }
  return fb;
}


/*
 *  CALCULATE GRAMS from ADC value  
 */
float calculateFSR_R(float value, int vinput, int adc, int R22){ 
  float sensorV; // VOLTS
  float sensorR; // OHMS
  
  if (value == 0) {value = 1;} // To avoid division
  
  sensorV=value*(float)vinput/(float)adc; // Calculate the VOLTS at FSR sensor
  sensorR=(((float)R22*(float)vinput)/sensorV)-(float)R22; // Calculate FSR RESISTANCE
  
return sensorR;
}




