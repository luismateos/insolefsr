//Robo-Mate project
//EXOSHOE - FSR sensors 
//------------------------------------------------
//Luis A. Mateos
//@IIT 25/04/2016


/*
 * ========================
 * INCLUDE
 * ========================
 */
//#include "definitions.h"
//#include "right.h"
////#include "left.h"
//#include "FSR.h"

#include <SoftwareSerial.h>


/*
 * ========================
 * SOFTWARE SERIAL
 * ========================
 */
// software serial #1: RX = digital pin 10, TX = digital pin 11
SoftwareSerial portLeft(10, 11);

// software serial #2: RX = digital pin 8, TX = digital pin 9
// on the Mega, use other pins instead, since 8 and 9 don't work on the Mega
SoftwareSerial portRight(8, 9);


/*
 * ========================
 * VARIABLES
 * ========================
 */
// byte LeftBuffer[4] = {0,0,0,0 }; 
byte inByteL = 0;
byte inByteL2 = 0;
byte inByteL3 = 0;
byte inByteL4 = 0;
// byte RightBuffer[4] = {0,0,0,0 };  
byte inByteR = 0;
byte inByteR2 = 0;
byte inByteR3 = 0;
byte inByteR4 = 0;

//counter of the bytes received by each software serial
int EchoIndexRight = 0;
int EchoIndexLeft = 0;

//flag to mark when the startbyte 0xAE is received
bool ReceivedLeftStartByte = false;
bool ReceivedRightStartByte = false;

//buffer (struct) to send to the robo-mate system 
byte buf[8] = {0,0,0,0,0,0,0,0};
byte startByte=0xAE;
byte messageType=0xFD; 
unsigned int combinedLeft;
unsigned int combinedRight;
unsigned char battery=95;
unsigned char checksum=0;

//flags to tell when we have data from a foot
int Rready=0;
int Lready=0;

//performance variables
unsigned long good = 0;
unsigned long bad = 0;
unsigned long goodR = 0;
unsigned long badR = 0;
unsigned long goodL = 0;
unsigned long badL = 0;
unsigned long counter = 0;
unsigned long counterL = 0;
unsigned long counterR = 0;
unsigned char port = 0;


/*
 * ========================
 * FUNCTION
 * ========================
 */

/** Clear the Left Buffer **/
void ClearLeftBuffer(void)
{
  EchoIndexLeft = 0;
//  LeftBuffer[0] = 0;
//  LeftBuffer[1] = 0;
//  LeftBuffer[2] = 0;
//  LeftBuffer[3] = 0;
}

/** Clear the Right Buffer **/
void ClearRightBuffer(void)
{
  EchoIndexRight = 0;
//  RightBuffer[0] = 0;
//  RightBuffer[1] = 0;
//  RightBuffer[2] = 0;
//  RightBuffer[3] = 0;
}



/*
 * ========================
 * SETUP
 * ========================
 */
void setup() {
  Serial.begin(115200);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }

  // Start each software serial port at 115200 or 57600
  portLeft.begin(115200);
  portRight.begin(115200);
}


/*
 * ========================
 * LOOP
 * ========================
 */
void loop() {
//if no data ready to send to robo-mate
if (Rready == 0){
  portRight.listen();
  while (portRight.available() > 0) { 
    //if we have not received the start byte 0xAE
    if (ReceivedRightStartByte == false) {  
        EchoIndexRight=1;
        inByteR = portRight.read();
    } 

    //if we received the start byte 0xAE
    if (inByteR == 0xAE) { 
        ReceivedRightStartByte = true;
        //while buffer not completed startByte/grams/crc
        while(EchoIndexRight < 5){
          EchoIndexRight++;
          if(EchoIndexRight == 2) {inByteR2 = portRight.read(); //Serial.print(":a:");Serial.print((unsigned char)inByteR2);
          }
          if(EchoIndexRight == 3) {inByteR3 = portRight.read(); //Serial.print(":b:");Serial.print((unsigned char)inByteR3);
                    combinedRight = inByteR3;//x_high;              //send x_high to Rightmost 8 bits
                    combinedRight = combinedRight<<8;         //shift x_high over to Rightmost 8 bits
                    combinedRight |= inByteR2;//x_low;                 //logical OR keeps x_high intact in combined and fills in                                   
                    //Serial.print(":x:");Serial.print((unsigned int)combinedRight);
//                    combinedRight2 = inByteR2;//x_high;              //send x_high to Rightmost 8 bits
//                    combinedRight2 = combinedRight2<<8;         //shift x_high over to Rightmost 8 bits
//                    combinedRight2 |= inByteR3;//x_low;                 //logical OR keeps x_high intact in combined and fills in                                   
          }
          if(EchoIndexRight == 4) {inByteR4 = portRight.read(); //Serial.print(":c:");Serial.print((unsigned char)inByteR4);
            ReceivedRightStartByte = false;
            checksum = 0;
            checksum = checksum ^ inByteR;
            checksum = checksum ^ inByteR3;
            checksum = checksum ^ inByteR2;
            counterR++;
              if (inByteR4 == checksum) { Rready=1; goodR++; good++; inByteR = 0;inByteR2 = 0;inByteR3 = 0;inByteR4 = 0;
              } else { badR++; bad++; inByteR = 0;inByteR2 = 0;inByteR3 = 0;inByteR4 = 0;
              }
//Serial.print("C:");Serial.print(counterR);Serial.print("Right:: OK:");Serial.print(goodR);Serial.print(" Right:: XX:");Serial.print(badR); Serial.println();          }
       }
    } 
  }
}
}




if (Lready == 0){
  portLeft.listen();
  while (portLeft.available() > 0) { 
    if (ReceivedLeftStartByte == false) {  
        EchoIndexLeft=1;
        inByteL = portLeft.read();
    } 
    
//    if ((unsigned char)inByteL == 174) { 
    if (inByteL == 0xAE) { 
        ReceivedLeftStartByte = true;
        while(EchoIndexLeft < 5){
          EchoIndexLeft++;
          if(EchoIndexLeft == 2) {inByteL2 = portLeft.read(); //Serial.print(":a:");Serial.print((unsigned char)inByteL2);
          }
          if(EchoIndexLeft == 3) {inByteL3 = portLeft.read(); //Serial.print(":b:");Serial.print((unsigned char)inByteL3);
                    combinedLeft = inByteL3;//x_high;              //send x_high to Leftmost 8 bits
                    combinedLeft = combinedLeft<<8;         //shift x_high over to leftmost 8 bits
                    combinedLeft |= inByteL2;//x_low;                 //logical OR keeps x_high intact in combined and fills in                                   
                    //Serial.print(":x:");Serial.print((unsigned int)combinedLeft);
          }
          if(EchoIndexLeft == 4) {inByteL4 = portLeft.read(); //Serial.print(":c:");Serial.print((unsigned char)inByteL4);
            ReceivedLeftStartByte = false;
            checksum = 0;
            checksum = checksum ^ inByteL;
            checksum = checksum ^ inByteL3;
            checksum = checksum ^ inByteL2;
            counterL++;
              if (inByteL4 == checksum) { Lready=1; goodL++; good++; inByteL = 0;inByteL2 = 0;inByteL3 = 0;inByteL4 = 0;
              } else { badL++; bad++; inByteL = 0;inByteL2 = 0;inByteL3 = 0;inByteL4 = 0;
              }
//              Serial.print("C:");Serial.print(counterL);Serial.print("LEFT:: OK:");Serial.print(goodL);Serial.print(" LEFT:: XX:");Serial.print(badL); Serial.println();
          }
       }
    } 
}

}


/*
 * IF we have data from the left and right prepare the buffer to be send to the robo-mate system
 */
  if ((Rready==1)&&(Lready==1))
  {
    checksum=0; // clear checksum
    
    //populate the buffer
    buf[0] = startByte;
    buf[1] = messageType;
    buf[2] = (byte)((combinedLeft >> 8) & 0xFF);
    buf[3] = (byte)(combinedLeft & 0xFF);
    buf[4] = (byte)((combinedRight >> 8) & 0xFF);
    buf[5] = (byte)(combinedRight & 0xFF);
    buf[6] = (byte)battery;
    
    //calculate the checksum
    checksum = checksum ^ buf[0];
    checksum = checksum ^ buf[1];
    checksum = checksum ^ buf[2];
    checksum = checksum ^ buf[3];
    checksum = checksum ^ buf[4];
    checksum = checksum ^ buf[5];
    checksum = checksum ^ buf[6];
    buf[7] = (byte)checksum;

    //send the buffer to the robo-mate system
    Serial.write(buf,sizeof(buf));
    
    // clear data ready flags
    Rready=0;
    Lready=0;
  }

}



